core = 7.x
api = 2

; page_load_progress
projects[page_load_progress][type] = "module"
projects[page_load_progress][download][type] = "git"
projects[page_load_progress][download][url] = "https://git.uwaterloo.ca/drupal-org/page_load_progress.git"
projects[page_load_progress][download][tag] = "7.x-1.3+2-uw_wcms"
projects[page_load_progress][subdir] = ""

; uw_important_dates_remote_site
projects[uw_important_dates_remote_site][type] = "module"
projects[uw_important_dates_remote_site][download][type] = "git"
projects[uw_important_dates_remote_site][download][url] = "https://git.uwaterloo.ca/wcms/uw_important_dates_remote_site.git"
projects[uw_important_dates_remote_site][download][tag] = "7.x-1.10"
projects[uw_important_dates_remote_site][subdir] = ""

; uw_ct_grad_program_search
projects[uw_ct_program_search][type] = "module"
projects[uw_ct_program_search][download][type] = "git"
projects[uw_ct_program_search][download][url] = "https://git.uwaterloo.ca/mur-dev/uw_ct_grad_program_search.git"
projects[uw_ct_program_search][download][tag] = "7.x-1.1"
projects[uw_ct_program_search][subdir] = ""

; uw_ft_grad_program_search
projects[uw_ft_program_search][type] = "module"
projects[uw_ft_program_search][download][type] = "git"
projects[uw_ft_program_search][download][url] = "https://git.uwaterloo.ca/mur-dev/uw_ft_grad_program_search.git"
projects[uw_ft_program_search][download][tag] = "7.x-1.1"
projects[uw_ft_program_search][subdir] = ""

; uw_studentbudget_calculator_gspa
projects[uw_studentbudget_calculator_gspa][type] = "module"
projects[uw_studentbudget_calculator_gspa][download][type] = "git"
projects[uw_studentbudget_calculator_gspa][download][url] = "https://git.uwaterloo.ca/mur-dev/uw_studentbudget_calculator_gspa.git"
projects[uw_studentbudget_calculator_gspa][download][tag] = "7.x-1.1"
projects[uw_studentbudget_calculator_gspa][subdir] = ""
